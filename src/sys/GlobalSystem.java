package sys;

import controllers.gui.TranslatorWindowFXMLController;
import controllers.gui.ViewListFXMLController;
import data.dictionary.NumberDictionary;
import data.dictionary.TermDictionary;
import javafx.util.Pair;
import modals.io.IOModal;
import modals.translators.TranslatorModal;
import data.PerformedTranslation;
import data.User;
import utils.LOG;

import java.net.PasswordAuthentication;

public class GlobalSystem {
    public enum LANG {ENG, VIE, FRA, SPA, ERR}
    private static boolean DEBUG_FLAG = true;

    private final static String SW_VER = "v.0.0.1a";

    private static LANG prefferredLanguage = LANG.VIE;
    private static GlobalSystem instance;

    private User activeUser;

    private TranslatorModal translatorModal;

    private GlobalSystem() {
        this.translatorModal = TranslatorModal.getInstance();
        this.activeUser = null;
    }

    public static GlobalSystem getInstance() {
        if (instance == null)
            instance = new GlobalSystem();
        return instance;
    }

    public String translate(final String input) {
        Pair<String, PerformedTranslation.TRANS_MODE> result =
                translatorModal.translate(input, activeUser.getCurrentUsingLanguage());
        if (result != null) {
            PerformedTranslation performedTranslation = new PerformedTranslation(
                    activeUser, input, result.getKey(), result.getValue());
            activeUser.addNewTranslation(performedTranslation);
            try {
                IOModal.getInstance().store(activeUser);
                IOModal.getInstance().store(performedTranslation);
                if (ViewListFXMLController.getInstance().getTotalPerformedTranslation() != null)
                    ViewListFXMLController.getInstance().getTotalPerformedTranslation().add(performedTranslation);
            } catch (Exception e) {
                LOG.w("Cannot store data!");
            }
            return performedTranslation.getResultContent();
        } else {
            return "Internal System Error!";
        }
    }

    public void createNewUser(
            String fName,
            String lName,
            String email,
            PasswordAuthentication authentication
    ) throws Exception {
        User newUser = new User(fName, lName, email, authentication, false);
        IOModal.getInstance().store(newUser);
    }

    private void createAdmin(
            String fName,
            String lName,
            String email,
            PasswordAuthentication authentication
    ) throws Exception {
        User newUser = new User(fName, lName, email, authentication, true);
        IOModal.getInstance().store(newUser);
    }

    public boolean login(PasswordAuthentication authentication) {
        User suspect = new User(authentication);
        try {
            IOModal.getInstance().load(suspect);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        if (suspect.authenticate(authentication)) {
            activeUser = suspect;
            return true;
        }
        return false;
    }

    public void logout() {
        activeUser = null;
        ViewListFXMLController.getInstance().logout();
        TranslatorWindowFXMLController.getInstance().logout();
    }

    public static sys.GlobalSystem.LANG getPrefferredLanguage() {
        return prefferredLanguage;
    }

    public User getActiveUser() {
        return activeUser;
    }

    public static String getSwVer() {
        return SW_VER;
    }

    public void setActiveUser(User activeUser) {
        this.activeUser = activeUser;
    }

    public static void setDebugFlag(boolean debugFlag) {
        DEBUG_FLAG = debugFlag;
    }

    public static boolean getDebugFlag() {
        return DEBUG_FLAG;
    }

    public void changeLanguage(LANG language) {
        activeUser.setCurrentUsingLanguage(language);
    }

    public void changeLanguage(String language) {
        LANG lang;
        if (language.equals(LANG.ENG.toString()))
            lang = LANG.ENG;
        else if (language.equals(LANG.FRA.toString()))
            lang = LANG.FRA;
        else if (language.equals(LANG.SPA.toString()))
            lang = LANG.SPA;
        else if (language.equals(LANG.VIE.toString()))
            lang = LANG.VIE;
        else
            lang = prefferredLanguage;
        activeUser.setCurrentUsingLanguage(lang);
        System.out.println("Language has been set to " + lang.toString());
    }

    public static void main(String[] args) throws Exception {
        GlobalSystem.getInstance().createAdmin(
                "Duy",
                "Dao",
                "daothanhduy305@gmail.com",
                new PasswordAuthentication("ebolo", "eboloyolo5653".toCharArray())
        );
    }

    public String getCurrentActiveLanguage() {
        return activeUser.getCurrentUsingLanguage().toString();
    }
}
