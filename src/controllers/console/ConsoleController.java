package controllers.console;

import controllers.gui.MainWindowFXMLController;
import controllers.gui.TranslatorWindowFXMLController;
import javafx.application.Platform;
import javafx.util.Pair;
import sys.GlobalSystem;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.function.Function;

/**
 * Created by ebolo on 3/28/17.
 */
public class ConsoleController {
    private static ConsoleController instance;
    private Map<String, ConsoleRunnable> api;

    private ConsoleController() {
        api = new HashMap<>(2);
        api.put("translate", arg -> System.out.println("Result = " + GlobalSystem.getInstance().translate(arg)));
        api.put("logout", arg -> Platform.runLater(() -> GlobalSystem.getInstance().logout()));
        api.put("admin-view", arg -> Platform.runLater(() -> TranslatorWindowFXMLController.getInstance().viewList()));
        api.put("show-gui", arg -> {
            GlobalSystem.getInstance().logout();
            Platform.runLater(() -> MainWindowFXMLController.getInstance().showGUI());
            Thread.currentThread().interrupt();
        });
        api.put("change-language", arg -> GlobalSystem.getInstance().changeLanguage(arg));
        api.put("show-current-language", arg -> System.out.println(
                "Current language: " + GlobalSystem.getInstance().getCurrentActiveLanguage()));
        api.put("show-available-language", arg -> {
            System.out.println("Available language(s):");
            for (GlobalSystem.LANG lang : GlobalSystem.LANG.values())
                System.out.print(lang.toString() + "  ");
            System.out.println();
        });
    }

    public static ConsoleController getInstance() {
        if (instance == null)
            instance = new ConsoleController();
        return instance;
    }

    public void startConsoleMode() {
        synchronized (System.in) {
            Scanner scanner = new Scanner(System.in);
            while (scanner.hasNextLine()) {
                String command = scanner.nextLine();
                commandExec(command);
            }
        }
    }

    private void commandExec(String fromString) {
        int space = fromString.trim().indexOf(' ');
        String command, arg;
        try {
            command = fromString.substring(0, space).trim();
            arg = fromString.substring(space + 1).trim();
        } catch (StringIndexOutOfBoundsException e) {
            command = fromString.trim();
            arg = "";
        }

        if (api.containsKey(command))
            api.get(command).run(arg);
        else
            System.out.println("Command not found!");
    }

    private interface ConsoleRunnable {
        public abstract void run(String arg);
    }
}
