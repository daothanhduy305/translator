package controllers.gui;

import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import sys.GlobalSystem;
import utils.LOG;

import java.net.PasswordAuthentication;

public class CreateUserFXMLController {
    private static CreateUserFXMLController instance;

    @FXML
    private TextField fNameTextField, lNameTextField, userIDTextField, emailTextField;
    @FXML
    private PasswordField passwordTextField, passwordTextFieldR;

    private CreateUserFXMLController() {
        //Some set up goes here
    }

    public static CreateUserFXMLController getInstance() {
        if (instance == null)
            instance = new CreateUserFXMLController();
        return instance;
    }

    @FXML
    private void createNewUser() {
        if (passwordTextField.getText().equals(passwordTextFieldR.getText())) {
            try {
                GlobalSystem.getInstance().createNewUser(
                        fNameTextField.getText(),
                        lNameTextField.getText(),
                        emailTextField.getText(),
                        new PasswordAuthentication(userIDTextField.getText(), passwordTextField.getText().toCharArray())
                );
                clear();
            } catch (Exception e) {
                e.printStackTrace();
                //TODO: Show error dialog
            }
        }
        // TODO fix here
    }

    @FXML
    private void clear() {
        fNameTextField.clear();
        lNameTextField.clear();
        userIDTextField.clear();
        passwordTextField.clear();
        passwordTextFieldR.clear();
        emailTextField.clear();
        MainWindowFXMLController.getInstance().showScreen(MainWindowFXMLController.WIN_TYPE.LOGIN);
    }
}
