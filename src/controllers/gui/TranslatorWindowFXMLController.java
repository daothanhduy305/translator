package controllers.gui;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import sys.GlobalSystem;
import utils.LOG;

import java.io.IOException;

/**
 * Created by ebolo on 3/29/17.
 */
public class TranslatorWindowFXMLController {
    private static TranslatorWindowFXMLController instance;
    private boolean isShowingData;
    private Stage dataStage;

    @FXML
    private TextArea inputTextArea, outputTextArea;
    @FXML
    private ComboBox<GlobalSystem.LANG> languageComboBox;

    private TranslatorWindowFXMLController() {
        //Some setup goes here
    }

    public static TranslatorWindowFXMLController getInstance() {
        if (instance == null)
            instance = new TranslatorWindowFXMLController();
        return instance;
    }

    @FXML
    private void translate() {
        outputTextArea.setText(GlobalSystem.getInstance().translate(inputTextArea.getText()));
    }

    @FXML
    public void logout() {
        inputTextArea.clear();
        outputTextArea.clear();
        GlobalSystem.getInstance().logout();
        MainWindowFXMLController.getInstance().showScreen(MainWindowFXMLController.WIN_TYPE.LOGIN);
        if (isShowingData)
            dataStage.close();
    }

    @FXML
    public void viewList() {
        if (!isShowingData) {
            Parent root;
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../../fxml/DataFXML.fxml"));

                DataFXMLController dataFXMLController = DataFXMLController.getInstance();
                ViewListFXMLController viewListFXMLController = ViewListFXMLController.getInstance();
                StatisticsFXMLController statisticsFXMLController = StatisticsFXMLController.getInstance();

                fxmlLoader.setControllerFactory(param -> {
                    if (param == DataFXMLController.class)
                        return dataFXMLController;
                    else if (param == ViewListFXMLController.class)
                        return viewListFXMLController;
                    else if (param == StatisticsFXMLController.class)
                        return statisticsFXMLController;
                    else {
                        System.out.println(param);
                        try {
                            return param.newInstance();
                        } catch (Exception exc) {
                            exc.printStackTrace();
                            throw new RuntimeException(exc);
                        }
                    }
                });

                root = fxmlLoader.load();

                dataFXMLController.setUp();

                dataStage = new Stage();
                dataStage.setTitle("Data view");
                dataStage.setScene(new Scene(root, 450, 450));
                dataStage.setOnCloseRequest(event -> {
                    dataStage.close();
                    isShowingData = !isShowingData;
                });
                isShowingData = !isShowingData;
                dataStage.show();
            } catch (IOException e) {
                LOG.e("Cannot show the stat window!");
                e.printStackTrace();
            }
        }
    }

    public void setLanguage() {
        if (languageComboBox.getItems().size() == 0)
            languageComboBox.getItems().addAll(GlobalSystem.LANG.ENG,
                    GlobalSystem.LANG.FRA,
                    GlobalSystem.LANG.SPA,
                    GlobalSystem.LANG.VIE
            );
        languageComboBox.setValue(GlobalSystem.getInstance().getActiveUser().getCurrentUsingLanguage());
        languageComboBox.valueProperty().addListener(
                (observable, oldValue, newValue) -> {
                    GlobalSystem.getInstance().changeLanguage(newValue);
                    LOG.d("User's language changed to " + newValue);
                });
    }
}
