package controllers.gui;

import controllers.console.ConsoleController;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import sys.GlobalSystem;
import utils.LOG;

import java.net.PasswordAuthentication;

/**
 * Created by ebolo on 3/28/17.
 */
public class LoginFXMLController {
    private static LoginFXMLController instance;

    @FXML private TextField usernameTextField;
    @FXML private PasswordField passwordTextField;
    @FXML private ComboBox<String> modeComboBox;

    private LoginFXMLController() {
        //Some set up goes here
    }

    public static LoginFXMLController getInstance() {
        if (instance == null)
            instance = new LoginFXMLController();
        return instance;
    }

    @FXML
    private void login() {
        // TODO: Add in various check conditions
        if (GlobalSystem.getInstance().login(new PasswordAuthentication(
                usernameTextField.getText(), passwordTextField.getText().toCharArray())
        )) {
            if (modeComboBox.getValue().equals("GUI")) {
                MainWindowFXMLController.getInstance().showScreen(MainWindowFXMLController.WIN_TYPE.TRANS);
                TranslatorWindowFXMLController.getInstance().setLanguage();
            } else {
                GlobalSystem.setDebugFlag(false);
                new Thread(() -> {
                    ConsoleController.getInstance().startConsoleMode();
                }).start();
                MainWindowFXMLController.getInstance().hideGUI();
            }
            clear();
        } else
            LOG.d("Login unsuccessfully");
    }

    @FXML
    private void createUser() {
        LOG.d("Start creating new user...");
        MainWindowFXMLController.getInstance().showScreen(MainWindowFXMLController.WIN_TYPE.NEW_USR);
    }

    @FXML
    private void clear() {
        usernameTextField.clear();
        passwordTextField.clear();
    }

    public void postInit() {
        modeComboBox.getItems().addAll("GUI", "Console");
        modeComboBox.setValue("GUI");
    }
}
