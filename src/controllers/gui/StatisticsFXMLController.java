package controllers.gui;

import data.Statistics;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

/**
 * Created by ebolo on 3/30/17.
 */
public class StatisticsFXMLController {
    private static StatisticsFXMLController instance;
    @FXML
    private Label numLabel, termsLabel, illegalLabel;

    private StatisticsFXMLController() {

    }

    public static StatisticsFXMLController getInstance() {
        if (instance == null)
            instance = new StatisticsFXMLController();
        return instance;
    }

    public void setup() {
        Statistics statistics = Statistics.getInstance();
        numLabel.textProperty().bind(Bindings.convert(statistics.numberTranslationPProperty()));
        termsLabel.textProperty().bind(Bindings.convert(statistics.termTranslationPProperty()));
        illegalLabel.textProperty().bind(Bindings.convert(statistics.illegalTranslationPProperty()));
    }
}
