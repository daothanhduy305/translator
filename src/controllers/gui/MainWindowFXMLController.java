package controllers.gui;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import utils.LOG;

import java.util.Scanner;

/**
 * Created by ebolo on 3/28/17.
 */
public class MainWindowFXMLController {
    public enum WIN_TYPE {LOGIN, NEW_USR, TRANS}
    private static MainWindowFXMLController instance;
    private Stage primaryStage;

    @FXML
    private StackPane screensHolder;

    private MainWindowFXMLController() {
        // Some set up goes here
    }

    public static MainWindowFXMLController getInstance() {
        if (instance == null)
            instance = new MainWindowFXMLController();
        return instance;
    }

    public void showScreen(WIN_TYPE screen) {
        for (Node node : screensHolder.getChildren())
            node.setVisible(false);
        int screenID = 0;
        switch (screen) {
            case LOGIN:
                screenID = 0;
                break;
            case NEW_USR:
                screenID = 1;
                break;
            case TRANS:
                screenID = 2;
                break;
        }
        LOG.d("Now showing screen = " + screen + " id = " + screenID);
        screensHolder.getChildren().get(screenID).setVisible(true);
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public void hideGUI() {
        primaryStage.hide();
    }

    public void showGUI() {
        primaryStage.show();
    }
}
