package controllers.gui;

import data.PerformedTranslation;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import modals.io.IOModal;
import sys.GlobalSystem;

/**
 * Created by ebolo on 3/29/17.
 */
public class ViewListFXMLController {
    private static ViewListFXMLController instance;
    @FXML
    private TableView<PerformedTranslation> translationTableVIew;
    @FXML
    private TableColumn<PerformedTranslation, String> inputCol, outputCol, userIDCol, langCol, typeCol;

    private ObservableList<PerformedTranslation> totalPerformedTranslation;

    private ViewListFXMLController() {

    }

    public static ViewListFXMLController getInstance() {
        if (instance == null)
            instance = new ViewListFXMLController();
        return instance;
    }

    public void setupData() {
        if (!GlobalSystem.getInstance().getActiveUser().isAdmin())
            translationTableVIew.setItems(GlobalSystem.getInstance().getActiveUser().getPerformedTranslationList());
        else {
            totalPerformedTranslation = FXCollections.observableArrayList();
            try {
                IOModal.getInstance().load(totalPerformedTranslation);
                translationTableVIew.setItems(this.totalPerformedTranslation);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        inputCol.setCellValueFactory(param -> param.getValue().originalContentProperty());
        outputCol.setCellValueFactory(param -> param.getValue().resultContentProperty());
        userIDCol.setCellValueFactory(param -> param.getValue().userIDProperty());
        langCol.setCellValueFactory(param -> param.getValue().languageProperty());
        typeCol.setCellValueFactory(param -> param.getValue().transModeProperty());
    }

    public ObservableList<PerformedTranslation> getTotalPerformedTranslation() {
        return totalPerformedTranslation;
    }

    public void setTotalPerformedTranslation(ObservableList<PerformedTranslation> totalPerformedTranslation) {
        this.totalPerformedTranslation = totalPerformedTranslation;
    }

    public void logout() {
        if (this.totalPerformedTranslation != null) {
            this.totalPerformedTranslation.clear();
            this.totalPerformedTranslation = null;
        }
    }
}
