package controllers.gui;

import data.PerformedTranslation;
import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import sys.GlobalSystem;

import java.util.List;

/**
 * Created by ebolo on 3/29/17.
 */
public class DataFXMLController {
    private static DataFXMLController instance;
    @FXML
    private Tab statTab;

    private DataFXMLController() {

    }

    public static DataFXMLController getInstance() {
        if (instance == null)
            instance = new DataFXMLController();
        return instance;
    }

    public void setUp() {
        ViewListFXMLController.getInstance().setupData();
        StatisticsFXMLController.getInstance().setup();
        if (GlobalSystem.getInstance().getActiveUser().isAdmin())
            statTab.setDisable(false);
        else
            statTab.setDisable(true);
    }
}
