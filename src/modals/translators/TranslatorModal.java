package modals.translators;

import data.PerformedTranslation;
import data.Statistics;
import data.dictionary.Dictionary;
import data.dictionary.NumberDictionary;
import data.dictionary.TermDictionary;
import javafx.util.Pair;
import sys.GlobalSystem;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class TranslatorModal {
    private static TranslatorModal instance;

    private TranslatorModal() {
        //Initialise data
    }

    public static TranslatorModal getInstance() {
        if (instance == null)
            instance = new TranslatorModal();
        return instance;
    }

    public Pair<String, PerformedTranslation.TRANS_MODE> translate(final String input, final GlobalSystem.LANG language) {
        ExecutorService jobsManager = Executors.newFixedThreadPool(5);
        Future<Pair<Boolean, String>> resultNumber = jobsManager.submit(
                new TranslateJob(NumberDictionary.getInstance(), input, language));
        Future<Pair<Boolean, String>> resultTerm = jobsManager.submit(
                new TranslateJob(TermDictionary.getInstance(), input, language));

        try {
            if (resultNumber != null || resultTerm != null) {
                if (resultNumber != null && resultNumber.get().getKey()) {
                    Statistics.getInstance().addNumberTranslation();
                    return new Pair<>(resultNumber.get().getValue(), PerformedTranslation.TRANS_MODE.NUM);
                }
                if (resultTerm != null && resultTerm.get().getKey()) {
                    Statistics.getInstance().addTermTranslation();
                    return new Pair<>(resultTerm.get().getValue(), PerformedTranslation.TRANS_MODE.TERM);
                }
            }
            Statistics.getInstance().addIllegalTranslation();
            return new Pair<>(resultNumber.get().getValue(), PerformedTranslation.TRANS_MODE.ILL);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    private class TranslateJob implements Callable<Pair<Boolean, String>> {
        private Dictionary dictionary;
        private String input;
        private GlobalSystem.LANG language;

        protected TranslateJob(Dictionary dictionary, String input, GlobalSystem.LANG language) {
            this.dictionary = dictionary;
            this.input = input;
            this.language = language;
        }

        @Override
        public Pair<Boolean, String> call() throws Exception {
            return new Pair<>(dictionary.hasTerm(input), dictionary.getMeaning(input, language));
        }
    }
}
