package modals.io;

import data.PerformedTranslation;
import interfaces.IOModalInterface;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class PerformedTranslationsIOModal implements IOModalInterface<List<PerformedTranslation>> {
    private static PerformedTranslationsIOModal instance;

    private PerformedTranslationsIOModal() {
        //Some setup might go here
    }

    public static PerformedTranslationsIOModal getInstance() {
        if (instance == null)
            instance = new PerformedTranslationsIOModal();
        return instance;
    }

    public void store(PerformedTranslation translation) throws Exception {
        List<PerformedTranslation> translations = new ArrayList<>(1);
        translations.add(translation);
        store(translations);
    }

    @Override
    public void store(List<PerformedTranslation> translations) throws Exception {
        File dataFile = new File("translationsDB");
        List<PerformedTranslation> performedTranslations = new ArrayList<>(10);
        if (!dataFile.exists()) {
            boolean result = dataFile.createNewFile();
            if (!result)
                throw new IOException();
        } else
            load(performedTranslations);

        try (
                FileOutputStream dataFileOutputStream = new FileOutputStream(dataFile);
                ObjectOutputStream dataOutputStream = new ObjectOutputStream(dataFileOutputStream)
        ){
            if (performedTranslations == null)
                performedTranslations = new ArrayList<>(10);
            performedTranslations.addAll(translations);
            dataOutputStream.writeObject(performedTranslations);
        } catch (IOException e) {
            throw e;
        }
    }

    @Override
    public void load(List<PerformedTranslation> translations) throws Exception {
        File dataFile = new File("translationsDB");
        try (
                FileInputStream dataFileInputStream= new FileInputStream(dataFile);
                ObjectInputStream dataInputStream = new ObjectInputStream(dataFileInputStream);
        ){
            translations.addAll((ArrayList<PerformedTranslation>) dataInputStream.readObject());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            throw e;
        }
    }
}
