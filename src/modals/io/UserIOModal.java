package modals.io;

import data.User;
import interfaces.IOModalInterface;
import utils.LOG;

import java.io.*;

/**
 * Created by ebolo on 3/29/17.
 */
public class UserIOModal implements IOModalInterface<User> {
    private static UserIOModal instance;

    private UserIOModal() {
        //SOme setup might go here
    }

    public static UserIOModal getInstance() {
        if (instance == null)
            instance = new UserIOModal();
        return instance;
    }

    @Override
    public void store(User user) throws IOException{
        File dataFile = new File(user.getUserID());
        try (
                FileOutputStream dataFileOutputStream = new FileOutputStream(dataFile);
                ObjectOutputStream dataOutputStream = new ObjectOutputStream(dataFileOutputStream)
        ){
            dataOutputStream.writeObject(user);
        } catch (IOException e) {
            throw e;
        }
    }

    @Override
    public void load(User user) throws IOException, ClassNotFoundException {
        File dataFile = new File(user.getUserID());
        try (
                FileInputStream dataFileInputStream= new FileInputStream(dataFile);
                ObjectInputStream dataInputStream = new ObjectInputStream(dataFileInputStream)
        ){
            user.copy((User) dataInputStream.readObject());

        } catch (IOException | ClassNotFoundException e) {
            LOG.e("Cannot load UserInfo data for user " + user.getUserID() + "!");
            throw e;
        }
    }
}
