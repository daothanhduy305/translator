package modals.io;

import data.PerformedTranslation;
import data.User;
import ecm.ECMSystem;
import ecm.data.XMLDocument;
import interfaces.IOModalInterface;
import javafx.collections.ObservableList;
import utils.LOG;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class IOModal implements IOModalInterface {
    private static IOModal instance;

    private UserIOModal userIOModal;
    private PerformedTranslationsIOModal performedTranslationsIOModal;

    private IOModal() {
        userIOModal = UserIOModal.getInstance();
        performedTranslationsIOModal = PerformedTranslationsIOModal.getInstance();
    }

    public static IOModal getInstance() {
        if (instance == null)
            instance = new IOModal();
        return instance;
    }

    @Override
    public void store(Object obj) throws Exception {
        if (obj != null) {
            if (obj instanceof User)
                userIOModal.store((User) obj);
            else if (obj instanceof PerformedTranslation)
                performedTranslationsIOModal.store((PerformedTranslation) obj);
        }
        else throw new NullPointerException();
    }

    @Override
    public void load(Object obj) throws Exception {
        if (obj != null) {
            if (obj instanceof User)
                userIOModal.load((User) obj);
        } else throw new NullPointerException();

    }

    public void load(List<PerformedTranslation> translations) throws Exception {
        if (translations != null) {
            performedTranslationsIOModal.load(translations);
        } else throw new NullPointerException();

    }
}
