import controllers.gui.CreateUserFXMLController;
import controllers.gui.LoginFXMLController;
import controllers.gui.MainWindowFXMLController;
import controllers.gui.TranslatorWindowFXMLController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sys.GlobalSystem;
import utils.GUIUtils;

public class Main extends Application{

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/MainWindowFXML.fxml"));

        MainWindowFXMLController mainWindowFXMLController = MainWindowFXMLController.getInstance();
        LoginFXMLController loginFXMLController = LoginFXMLController.getInstance();
        CreateUserFXMLController createUserFXMLController = CreateUserFXMLController.getInstance();
        TranslatorWindowFXMLController translatorWindowFXMLController = TranslatorWindowFXMLController.getInstance();

        fxmlLoader.setControllerFactory(param -> {
            if (param == MainWindowFXMLController.class)
                return mainWindowFXMLController;
            else if (param == LoginFXMLController.class)
                return loginFXMLController;
            else if (param == CreateUserFXMLController.class)
                return createUserFXMLController;
            else if (param == TranslatorWindowFXMLController.class)
                return translatorWindowFXMLController;
            else {
                System.out.println(param);
                try {
                    return param.newInstance();
                } catch (Exception exc) {
                    exc.printStackTrace();
                    throw new RuntimeException(exc);
                }
            }
        });

        Parent root = fxmlLoader.load();

        loginFXMLController.postInit();
        mainWindowFXMLController.setPrimaryStage(primaryStage);

        primaryStage.setTitle("Translator Software " + GlobalSystem.getSwVer());
        primaryStage.setScene(new Scene(root, GUIUtils.getScreenWidth(), GUIUtils.getScreenHeight()));
        primaryStage.setMaximized(true);

        mainWindowFXMLController.showScreen(MainWindowFXMLController.WIN_TYPE.LOGIN);
        Platform.setImplicitExit(false);
        primaryStage.setOnCloseRequest(event -> System.exit(0));
        primaryStage.show();
        //primaryStage.setOnCloseRequest(event -> shutdown());
    }

    public static void main(String[] args) {
        launch(args);
    }
}
