package ecm;

import ecm.data.XMLDocument;
import ecm.data.XMLStatus;

/**
 * This is just abstract class to demonstrate ECM System
 */
public class ECMSystem {
    public static XMLStatus store(XMLDocument inputDoc) {
        return new XMLStatus();
    }
}
