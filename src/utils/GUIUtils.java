package utils;

import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;

/**
 * Created by ebolo on 3/28/17.
 */
public class GUIUtils {

    private static final Rectangle2D monitorShape = Screen.getPrimary().getVisualBounds();

    public static double getScreenHeight() {
        return monitorShape.getHeight();
    }

    public static double getScreenWidth() {
        return monitorShape.getWidth();
    }
}