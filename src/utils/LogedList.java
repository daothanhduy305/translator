package utils;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class LogedList<T> implements List<T> {
    private List innerList;

    public LogedList(List<T> boundedList) {
        innerList = boundedList;
    }

    @Override
    public boolean add(Object o) {
        System.out.println("LOG: now performing the addition of an object");
        long startTime = System.currentTimeMillis();
        boolean result = innerList.add(o);
        System.out.println("LOG: total processing time = " + (System.currentTimeMillis() - startTime) + "ms");
        return result;
    }

    @Override
    public void add(int index, Object element) {
        System.out.println("LOG: now performing the addition of an object");
        long startTime = System.currentTimeMillis();
        innerList.add(index, element);
        System.out.println("LOG: total processing time = " + (System.currentTimeMillis() - startTime) + "ms");
    }

    @Override
    public boolean addAll(Collection c) {
        System.out.println("LOG: now performing the addition of an object");
        long startTime = System.currentTimeMillis();
        boolean result = innerList.addAll(c);
        System.out.println("LOG: total processing time = " + (System.currentTimeMillis() - startTime) + "ms");
        return result;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        System.out.println("LOG: now performing the addition of an object");
        long startTime = System.currentTimeMillis();
        boolean result = innerList.addAll(index, c);
        System.out.println("LOG: total processing time = " + (System.currentTimeMillis() - startTime) + "ms");
        return result;
    }

    @Override
    public int size() {
        return innerList.size();
    }

    @Override
    public boolean isEmpty() {
        return innerList.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return innerList.contains(o);
    }

    @Override
    public Iterator iterator() {
        return innerList.iterator();
    }

    @Override
    public Object[] toArray() {
        return innerList.toArray();
    }

    @Override
    public boolean remove(Object o) {
        return innerList.remove(o);
    }

    @Override
    public void clear() {
        innerList.clear();
    }

    @Override
    public T get(int index) {
        return (T) innerList.get(index);
    }

    @Override
    public Object set(int index, Object element) {
        return innerList.set(index, element);
    }

    @Override
    public T remove(int index) {
        return (T) innerList.remove(index);
    }

    @Override
    public int indexOf(Object o) {
        return innerList.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return innerList.lastIndexOf(o);
    }

    @Override
    public ListIterator listIterator() {
        return innerList.listIterator();
    }

    @Override
    public ListIterator listIterator(int index) {
        return innerList.listIterator(index);
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        return innerList.subList(fromIndex, toIndex);
    }

    @Override
    public boolean retainAll(Collection c) {
        return innerList.retainAll(c);
    }

    @Override
    public boolean removeAll(Collection c) {
        return innerList.removeAll(c);
    }

    @Override
    public boolean containsAll(Collection c) {
        return innerList.containsAll(c);
    }

    @Override
    public Object[] toArray(Object[] a) {
        return innerList.toArray(a);
    }
}
