package utils;

import sys.GlobalSystem;

/**
 * Created by ebolo on 3/28/17.
 */
public class LOG {
    private static final String LOG_TAG = "LOG:";

    public static void d(String message) {
        if (GlobalSystem.getDebugFlag())
            System.out.println(LOG_TAG + " (debug) " + message);
    }

    public static void e(String message) {
        if (GlobalSystem.getDebugFlag())
            System.out.println(LOG_TAG + " (error) " + message);
    }

    public static void w(String message) {
        if (GlobalSystem.getDebugFlag())
            System.out.println(LOG_TAG + " (warning) " + message);
    }
}
