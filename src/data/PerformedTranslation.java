package data;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sys.GlobalSystem;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Created by Thanh Binh on 21/03/2017.
 */
public class PerformedTranslation implements Serializable {
    public enum TRANS_MODE {NUM, TERM, ILL}

    private StringProperty originalContent, resultContent, userID, language, transMode;
    /*private GlobalSystem.LANG language;
    private TRANS_MODE transMode;*/

    public PerformedTranslation(User user, String originalContent, String resultContent, TRANS_MODE transMode) {
        this.userID = new SimpleStringProperty(user.getUserID());
        this.originalContent = new SimpleStringProperty(originalContent);
        this.resultContent = new SimpleStringProperty(resultContent);
        this.language = new SimpleStringProperty(user.getCurrentUsingLanguage().toString());
        this.transMode = new SimpleStringProperty(transMode.toString());
    }

    public String getUserID() {
        return userID.get();
    }


    public String getOriginalContent() {
        return originalContent.get();
    }

    public String getResultContent() {
        return resultContent.get();
    }

    public StringProperty originalContentProperty() {
        return originalContent;
    }

    public StringProperty resultContentProperty() {
        return resultContent;
    }

    public StringProperty userIDProperty() {
        return userID;
    }

    public String getLanguage() {
        return language.get();
    }

    public StringProperty languageProperty() {
        return language;
    }

    public String getTransMode() {
        return transMode.get();
    }

    public StringProperty transModeProperty() {
        return transMode;
    }

    private void writeObject(ObjectOutputStream s) throws IOException {
        s.writeUTF(originalContent.get());
        s.writeUTF(resultContent.get());
        s.writeUTF(userID.get());
        s.writeUTF(language.get());
        s.writeUTF(transMode.get());
    }

    private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
        originalContent = new SimpleStringProperty(s.readUTF());
        resultContent = new SimpleStringProperty(s.readUTF());
        userID = new SimpleStringProperty(s.readUTF());
        language = new SimpleStringProperty(s.readUTF());
        transMode = new SimpleStringProperty(s.readUTF());
    }
}
