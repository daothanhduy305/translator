package data;

import interfaces.IOModalInterface;
import javafx.application.Platform;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import sys.GlobalSystem;
import utils.LOG;

import java.io.*;
import java.util.ArrayList;

public class Statistics implements Serializable, IOModalInterface<Statistics> {
    private static Statistics instance;

    private long numberTranslation;
    private long termTranslation;
    private long illegalTranslation;

    private SimpleIntegerProperty numberTranslationP;
    private SimpleIntegerProperty termTranslationP;
    private SimpleIntegerProperty illegalTranslationP;

    private Statistics() {
        try {
            load();
        } catch (Exception e) {
            numberTranslation = 0;
            termTranslation = 0;
            illegalTranslation = 0;

            numberTranslationP = new SimpleIntegerProperty(0);
            termTranslationP = new SimpleIntegerProperty(0);
            illegalTranslationP = new SimpleIntegerProperty(0);
        }
    }

    public static Statistics getInstance() {
        if (instance == null) {
            instance = new Statistics();
        }
        return instance;
    }

    public void addNumberTranslation() {
        this.numberTranslation++;
        update();
        store();
    }

    public void addTermTranslation() {
        this.termTranslation++;
        update();
        store();
    }

    public void addIllegalTranslation() {
        this.illegalTranslation++;
        update();
        store();
    }

    @Override
    public void store(Statistics statistics) throws Exception {
        File dataFile = new File("statDB");
        try (
                FileOutputStream dataFileOutputStream = new FileOutputStream(dataFile);
                ObjectOutputStream dataOutputStream = new ObjectOutputStream(dataFileOutputStream)
        ){
            dataOutputStream.writeObject(statistics);
        } catch (IOException e) {
            throw e;
        }
    }

    public void store() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    store(instance);
                } catch (Exception e) {
                    LOG.w("Can't save statistics!");
                }
            }
        }).start();
    }

    @Deprecated
    @Override
    public void load(Statistics obj) throws Exception {
        //No way this should go public
    }

    private void load() throws Exception {
        File dataFile = new File("statDB");
        try (
                FileInputStream dataFileInputStream= new FileInputStream(dataFile);
                ObjectInputStream dataInputStream = new ObjectInputStream(dataFileInputStream)
        ){
            Statistics loadedStat = (Statistics) dataInputStream.readObject();
            this.numberTranslation = loadedStat.numberTranslation;
            this.termTranslation = loadedStat.termTranslation;
            this.illegalTranslation = loadedStat.illegalTranslation;

            this.numberTranslationP = loadedStat.numberTranslationP;
            this.termTranslationP = loadedStat.termTranslationP;
            this.illegalTranslationP = loadedStat.illegalTranslationP;
        } catch (IOException | ClassNotFoundException e) {
            LOG.e("Cannot load Statistics data!");
            throw e;
        }
    }

    private void update() {
        Runnable updateRunnable = new Runnable() {
            @Override
            public void run() {
                if (numberTranslation + termTranslation + illegalTranslation != 0) {
                    numberTranslationP.set((int) ((numberTranslation * 100) / (numberTranslation + termTranslation + illegalTranslation)));
                    termTranslationP.set((int) ((termTranslation * 100) / (numberTranslation + termTranslation + illegalTranslation)));
                    illegalTranslationP.set(100 - numberTranslationP.get() - termTranslationP.get());
                }
            }
        };
        if (Platform.isFxApplicationThread())
            updateRunnable.run();
        else
            Platform.runLater(() -> updateRunnable.run());
    }

    private void writeObject(ObjectOutputStream s) throws IOException {
        s.writeLong(numberTranslation);
        s.writeLong(termTranslation);
        s.writeLong(illegalTranslation);

        s.writeInt(numberTranslationP.get());
        s.writeInt(termTranslationP.get());
        s.writeInt(illegalTranslationP.get());
    }

    private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
        this.numberTranslation = s.readLong();
        this.termTranslation = s.readLong();
        this.illegalTranslation = s.readLong();

        this.numberTranslationP = new SimpleIntegerProperty(s.readInt());
        this.termTranslationP = new SimpleIntegerProperty(s.readInt());
        this.illegalTranslationP = new SimpleIntegerProperty(s.readInt());
    }

    public int getNumberTranslationP() {
        return numberTranslationP.get();
    }

    public SimpleIntegerProperty numberTranslationPProperty() {
        return numberTranslationP;
    }

    public int getTermTranslationP() {
        return termTranslationP.get();
    }

    public SimpleIntegerProperty termTranslationPProperty() {
        return termTranslationP;
    }

    public int getIllegalTranslationP() {
        return illegalTranslationP.get();
    }

    public SimpleIntegerProperty illegalTranslationPProperty() {
        return illegalTranslationP;
    }
}
