package data;

import com.sun.istack.internal.Nullable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sys.GlobalSystem;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.PasswordAuthentication;
import java.util.ArrayList;

/**
 * Created by Thanh Binh on 21/03/2017.
 */
public class User implements Serializable {
    private enum USR_TYPE {NORMAL, ADMIN};

    private String fName, lName, email, userID;
    private char[] password;
    private sys.GlobalSystem.LANG currentUsingLanguage;
    private USR_TYPE usrType;

    private ObservableList<PerformedTranslation> performedTranslationList;

    public User(PasswordAuthentication authentication) {
        this("", "", "", authentication, false);
    }

    public User(String fName, String lName, String email, PasswordAuthentication authentication, boolean admin) {
        this.fName = fName;
        this.lName = lName;
        this.email = email;
        this.userID = authentication.getUserName();
        this.password = authentication.getPassword();
        this.currentUsingLanguage = GlobalSystem.getPrefferredLanguage();

        this.usrType = admin? USR_TYPE.ADMIN : USR_TYPE.NORMAL;

        performedTranslationList = FXCollections.observableArrayList();
    }

    public void addNewTranslation(PerformedTranslation performedTranslation) {
        performedTranslationList.add(performedTranslation);
    }

    public void requestNumberTranslate(final String input, GlobalSystem globalSystem) {
        globalSystem.translate(input);
    }

    public ObservableList<PerformedTranslation> getPerformedTranslationList() {
        return performedTranslationList;
    }

    public void printPerformedTranslations() {
        //Implement
    }

    public sys.GlobalSystem.LANG getCurrentUsingLanguage() {
        return currentUsingLanguage;
    }

    public void setCurrentUsingLanguage(sys.GlobalSystem.LANG currentUsingLanguage) {
        this.currentUsingLanguage = currentUsingLanguage;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean authenticate(PasswordAuthentication authentication) {
        return (
                this.userID.equals(authentication.getUserName())
                        && (new String(this.password).equals(new String (authentication.getPassword())))
        );
    }

    public String getUserID() {
        return this.userID;
    }

    public USR_TYPE getUsrType() {
        return usrType;
    }

    public void setAdmin() {
        this.usrType = USR_TYPE.ADMIN;
    }

    public void provokeAdmin() {
        this.usrType = USR_TYPE.NORMAL;
    }

    public boolean isAdmin() {
        return (this.usrType == USR_TYPE.ADMIN);
    }

    public void copy(User source) {
        this.fName = source.fName;
        this.lName = source.lName;
        this.email = source.email;
        this.userID = source.userID;
        this.password = source.password;
        this.currentUsingLanguage = source.currentUsingLanguage;
        this.usrType = source.usrType;

        this.performedTranslationList = source.performedTranslationList;
    }

    private void writeObject(ObjectOutputStream s) throws IOException {
        s.writeUTF(this.fName);
        s.writeUTF(this.lName);
        s.writeUTF(this.email);
        s.writeUTF(this.userID);
        s.writeObject(this.password);
        s.writeObject(this.currentUsingLanguage);
        s.writeObject(this.usrType);
        s.writeObject(new ArrayList<PerformedTranslation>(this.performedTranslationList));
    }

    private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
        this.fName = s.readUTF();
        this.lName = s.readUTF();
        this.email = s.readUTF();
        this.userID = s.readUTF();

        this.password = (char[]) s.readObject();
        this.currentUsingLanguage = (GlobalSystem.LANG) s.readObject();
        this.usrType = (USR_TYPE) s.readObject();
        this.performedTranslationList = FXCollections.observableArrayList(
                (ArrayList<PerformedTranslation>) s.readObject());
    }
}
