package data.dictionary;

import sys.GlobalSystem;

import java.util.Map;
import java.util.TreeMap;

public class Word {
    private String content;
    private Map<GlobalSystem.LANG, String> foreignWords;

    public Word(String content) {
        this.content = content;
        foreignWords = new TreeMap<>();
    }

    public Word(String content, GlobalSystem.LANG language, String foreignWord) {
        this(content);
        this.addForeignWord(language, foreignWord);
    }

    public void addForeignWord(GlobalSystem.LANG language, String foreignWord) {
        foreignWords.put(language, foreignWord);
    }

    public String getTranslation(GlobalSystem.LANG language) {
        return foreignWords.get(language);
    }
}
