package data.dictionary;

import sys.GlobalSystem;

import java.util.TreeMap;

/**
 * Created by ebolo on 3/26/17.
 */
public class NumberDictionary extends Dictionary {
    private static NumberDictionary instance;

    private NumberDictionary() {
        data = new TreeMap<>();

        this.addNewterm("eins", GlobalSystem.LANG.ENG, "one");
        this.addNewterm("eins", GlobalSystem.LANG.FRA, "un");
        this.addNewterm("eins", GlobalSystem.LANG.SPA, "uno");
        this.addNewterm("eins", GlobalSystem.LANG.VIE, "một");

        this.addNewterm("zwei", GlobalSystem.LANG.ENG, "two");
        this.addNewterm("zwei", GlobalSystem.LANG.FRA, "deux");
        this.addNewterm("zwei", GlobalSystem.LANG.SPA, "dos");
        this.addNewterm("zwei", GlobalSystem.LANG.VIE, "hai");

        this.addNewterm("drei", GlobalSystem.LANG.ENG, "three");
        this.addNewterm("drei", GlobalSystem.LANG.FRA, "trois");
        this.addNewterm("drei", GlobalSystem.LANG.SPA, "tres");
        this.addNewterm("drei", GlobalSystem.LANG.VIE, "ba");

        this.addNewterm("vier", GlobalSystem.LANG.ENG, "four");
        this.addNewterm("vier", GlobalSystem.LANG.FRA, "quatre");
        this.addNewterm("vier", GlobalSystem.LANG.SPA, "cuatro");
        this.addNewterm("vier", GlobalSystem.LANG.VIE, "bốn");

        this.addNewterm("fünf", GlobalSystem.LANG.ENG, "five");
        this.addNewterm("fünf", GlobalSystem.LANG.FRA, "cinq");
        this.addNewterm("fünf", GlobalSystem.LANG.SPA, "cinco");
        this.addNewterm("fünf", GlobalSystem.LANG.VIE, "năm");

        this.addNewterm("sechs", GlobalSystem.LANG.ENG, "six");
        this.addNewterm("sechs", GlobalSystem.LANG.FRA, "six");
        this.addNewterm("sechs", GlobalSystem.LANG.SPA, "seis");
        this.addNewterm("sechs", GlobalSystem.LANG.VIE, "sáu");

        this.addNewterm("sieben", GlobalSystem.LANG.ENG, "seven");
        this.addNewterm("sieben", GlobalSystem.LANG.FRA, "sept");
        this.addNewterm("sieben", GlobalSystem.LANG.SPA, "siete");
        this.addNewterm("sieben", GlobalSystem.LANG.VIE, "bảy");

        this.addNewterm("acht", GlobalSystem.LANG.ENG, "eight");
        this.addNewterm("acht", GlobalSystem.LANG.FRA, "huit");
        this.addNewterm("acht", GlobalSystem.LANG.SPA, "ocho");
        this.addNewterm("acht", GlobalSystem.LANG.VIE, "tám");

        this.addNewterm("neun", GlobalSystem.LANG.ENG, "nine");
        this.addNewterm("neun", GlobalSystem.LANG.FRA, "neuf");
        this.addNewterm("neun", GlobalSystem.LANG.SPA, "nueve");
        this.addNewterm("neun", GlobalSystem.LANG.VIE, "chín");

        this.addNewterm("zehn", GlobalSystem.LANG.ENG, "ten");
        this.addNewterm("zehn", GlobalSystem.LANG.FRA, "dix");
        this.addNewterm("zehn", GlobalSystem.LANG.SPA, "diez");
        this.addNewterm("zehn", GlobalSystem.LANG.VIE, "mười");

        this.addNewterm("elf", GlobalSystem.LANG.ENG, "eleven");
        this.addNewterm("elf", GlobalSystem.LANG.FRA, "onze");
        this.addNewterm("elf", GlobalSystem.LANG.SPA, "once");
        this.addNewterm("elf", GlobalSystem.LANG.VIE, "mười một");

        this.addNewterm("zwölf", GlobalSystem.LANG.ENG, "twelve");
        this.addNewterm("zwölf", GlobalSystem.LANG.FRA, "douze");
        this.addNewterm("zwölf", GlobalSystem.LANG.SPA, "doce");
        this.addNewterm("zwölf", GlobalSystem.LANG.VIE, "mười hai");

        this.addNewterm("dreizehn", GlobalSystem.LANG.ENG, "thirteen");
        this.addNewterm("dreizehn", GlobalSystem.LANG.FRA, "treize");
        this.addNewterm("dreizehn", GlobalSystem.LANG.SPA, "trece");
        this.addNewterm("dreizehn", GlobalSystem.LANG.VIE, "mười ba");

        this.addNewterm("vierzehn", GlobalSystem.LANG.ENG, "fourteen");
        this.addNewterm("vierzehn", GlobalSystem.LANG.FRA, "quatorze");
        this.addNewterm("vierzehn", GlobalSystem.LANG.SPA, "catorce");
        this.addNewterm("vierzehn", GlobalSystem.LANG.VIE, "mười bốn");

        this.addNewterm("fünfzehn", GlobalSystem.LANG.ENG, "fifteen");
        this.addNewterm("fünfzehn", GlobalSystem.LANG.FRA, "quinze");
        this.addNewterm("fünfzehn", GlobalSystem.LANG.SPA, "quince");
        this.addNewterm("fünfzehn", GlobalSystem.LANG.VIE, "mười lăm");

        this.addNewterm("sechzehn", GlobalSystem.LANG.ENG, "sixteen");
        this.addNewterm("sechzehn", GlobalSystem.LANG.FRA, "seize");
        this.addNewterm("sechzehn", GlobalSystem.LANG.SPA, "dieciséis");
        this.addNewterm("sechzehn", GlobalSystem.LANG.VIE, "mười sáu");

        this.addNewterm("siebzehn", GlobalSystem.LANG.ENG, "seventeen");
        this.addNewterm("siebzehn", GlobalSystem.LANG.FRA, "dix-sept");
        this.addNewterm("siebzehn", GlobalSystem.LANG.SPA, "diecisiete");
        this.addNewterm("siebzehn", GlobalSystem.LANG.VIE, "mười bảy");

        this.addNewterm("achtzehn", GlobalSystem.LANG.ENG, "eighteen");
        this.addNewterm("achtzehn", GlobalSystem.LANG.FRA, "dix-huit");
        this.addNewterm("achtzehn", GlobalSystem.LANG.SPA, "dieciocho");
        this.addNewterm("achtzehn", GlobalSystem.LANG.VIE, "mười tám");

        this.addNewterm("neunzehn", GlobalSystem.LANG.ENG, "nineteen");
        this.addNewterm("neunzehn", GlobalSystem.LANG.FRA, "dix-neuf");
        this.addNewterm("neunzehn", GlobalSystem.LANG.SPA, "diecinueve");
        this.addNewterm("neunzehn", GlobalSystem.LANG.VIE, "mười chín");

        this.addNewterm("zwanzig", GlobalSystem.LANG.ENG, "twenty");
        this.addNewterm("zwanzig", GlobalSystem.LANG.FRA, "vingt");
        this.addNewterm("zwanzig", GlobalSystem.LANG.SPA, "veinte");
        this.addNewterm("zwanzig", GlobalSystem.LANG.VIE, "hai mươi");
    }

    public static NumberDictionary getInstance() {
        if (instance == null)
            instance = new NumberDictionary();
        return instance;
    }
}
