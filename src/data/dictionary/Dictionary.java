package data.dictionary;

import sys.GlobalSystem;
import utils.LOG;

import java.util.Map;

/**
 * Created by ebolo on 3/26/17.
 */
public class Dictionary {
    protected Map<String, Word> data;

    protected void addNewterm(String term, GlobalSystem.LANG language, String meaning) {
        assert data != null : "Data has not been initialed!";
        if (data.containsKey(term))
            data.get(term).addForeignWord(language, meaning);
        else
            data.put(term, new Word(term, language, meaning));
    }

    public String getMeaning(String term, GlobalSystem.LANG language) {
        assert data != null : "Data has not been initialed!";
        String meaning = data.containsKey(term)? data.get(term).getTranslation(language) : null;
        LOG.d("New Translation received, term = " + term + " LANG = " + language);
        return meaning != null? meaning : "Unsuccessful translation!";
    }

    public boolean hasTerm(String term) {
        assert data != null : "Data has not been initialed!";
        return data.containsKey(term);
    }
}
