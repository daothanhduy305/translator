package data.dictionary;

import sys.GlobalSystem;

import java.util.TreeMap;

/**
 * Created by ebolo on 3/26/17.
 */
public class TermDictionary extends Dictionary {
    private static TermDictionary instance;

    private TermDictionary() {
        data = new TreeMap<>();

        this.addNewterm("hallo", GlobalSystem.LANG.ENG, "hello");
        this.addNewterm("hallo", GlobalSystem.LANG.FRA, "bonjour");
        this.addNewterm("hallo", GlobalSystem.LANG.SPA, "¡Hola");
        this.addNewterm("hallo", GlobalSystem.LANG.VIE, "chào");

        this.addNewterm("tschüss", GlobalSystem.LANG.ENG, "goobye");
        this.addNewterm("tschüss", GlobalSystem.LANG.FRA, "au revoir");
        this.addNewterm("tschüss", GlobalSystem.LANG.SPA, "hasta luego");
        this.addNewterm("tschüss", GlobalSystem.LANG.VIE, "tạm biệt");

        this.addNewterm("essen", GlobalSystem.LANG.ENG, "eat");
        this.addNewterm("essen", GlobalSystem.LANG.FRA, "manger");
        this.addNewterm("essen", GlobalSystem.LANG.SPA, "comer");
        this.addNewterm("essen", GlobalSystem.LANG.VIE, "ăn");

        this.addNewterm("spielen", GlobalSystem.LANG.ENG, "play");
        this.addNewterm("spielen", GlobalSystem.LANG.FRA, "jouer");
        this.addNewterm("spielen", GlobalSystem.LANG.SPA, "jugar");
        this.addNewterm("spielen", GlobalSystem.LANG.VIE, "chơi");

        this.addNewterm("trinken", GlobalSystem.LANG.ENG, "drink");
        this.addNewterm("trinken", GlobalSystem.LANG.FRA, "boisson");
        this.addNewterm("trinken", GlobalSystem.LANG.SPA, "bebida");
        this.addNewterm("trinken", GlobalSystem.LANG.VIE, "uống");

        this.addNewterm("kennen", GlobalSystem.LANG.ENG, "know");
        this.addNewterm("kennen", GlobalSystem.LANG.FRA, "savoir");
        this.addNewterm("kennen", GlobalSystem.LANG.SPA, "conocer");
        this.addNewterm("kennen", GlobalSystem.LANG.VIE, "biết");

        this.addNewterm("ich bin Student", GlobalSystem.LANG.ENG, "i am a student");
        this.addNewterm("ich bin Student", GlobalSystem.LANG.FRA, "Je suis un étudiant");
        this.addNewterm("ich bin Student", GlobalSystem.LANG.SPA, "Soy un estudiante");
        this.addNewterm("ich bin Student", GlobalSystem.LANG.VIE, "tôi là học sinh");
    }

    public static TermDictionary getInstance() {
        if (instance == null)
            instance = new TermDictionary();
        return instance;
    }
}
