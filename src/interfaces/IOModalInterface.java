package interfaces;

import java.io.IOException;

/**
 * Created by ebolo on 3/29/17.
 */
public interface IOModalInterface<E> {
    public abstract void store(E obj) throws Exception;
    public abstract void load(E obj) throws Exception;
}
