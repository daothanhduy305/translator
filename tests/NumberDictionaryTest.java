import data.dictionary.NumberDictionary;
import org.junit.Test;
import sys.GlobalSystem;

import static org.junit.Assert.*;

/**
 * Created by ebolo on 3/29/17.
 */
public class NumberDictionaryTest {
    private NumberDictionary dictionary;

    @Test
    public void testTranslate() {
        dictionary = NumberDictionary.getInstance();
        assertEquals("one", dictionary.getMeaning("eins", GlobalSystem.LANG.ENG));
    }
}
