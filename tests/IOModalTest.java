import data.PerformedTranslation;
import data.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import modals.io.IOModal;
import org.junit.Test;
import sys.GlobalSystem;

import java.net.PasswordAuthentication;

import static org.junit.Assert.*;

/**
 * Created by ebolo on 3/29/17.
 */
public class IOModalTest {
    @Test
    public void storeLoadTransTest() throws Exception{
        User user1 = new User("a", "a", "a", new PasswordAuthentication("a", "a".toCharArray()), false);

        GlobalSystem globalSystem = GlobalSystem.getInstance();

        globalSystem.setActiveUser(user1);

        GlobalSystem.getInstance().translate("eins");
        GlobalSystem.getInstance().translate("eins");
        GlobalSystem.getInstance().translate("eins");

        User user2 = new User(new PasswordAuthentication("a", "a".toCharArray()));
        IOModal.getInstance().load(user2);

        //assertEquals(user1, user2);
        assertEquals(3, user2.getPerformedTranslationList().size());

        ObservableList<PerformedTranslation> test = FXCollections.observableArrayList(GlobalSystem.getInstance().getActiveUser().getPerformedTranslationList());
        test.add(new PerformedTranslation(GlobalSystem.getInstance().getActiveUser(), "eins", "one", PerformedTranslation.TRANS_MODE.NUM));
        //GlobalSystem.getInstance().translate("eins");
        assertNotEquals(4, GlobalSystem.getInstance().getActiveUser().getPerformedTranslationList().size());
    }
}
