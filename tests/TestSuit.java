import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by ebolo on 3/29/17.
 */

@RunWith(Suite.class)
@Suite.SuiteClasses(
        {NumberDictionaryTest.class,
                IOModalTest.class}
        )

public class TestSuit {
}
